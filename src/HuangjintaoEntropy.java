import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class HuangjintaoEntropy {
    public static void main(String[] args) throws IOException {
        //定义一个Map集合储存数据，字符为键（Character类型），出现次数为值（Integer类型）
        HashMap<Character, Integer> map = new HashMap<>();
        File f1 = new File("C:\\Users\\29553\\IdeaProjects\\homework\\javahomework\\src\\test.txt");
        //定义一个字符缓冲输入流更快的读取数据
        BufferedReader br = new BufferedReader(new FileReader(f1));
        String len;
        //初始化文本总字符数
        int sum = 0;
        while ((len = br.readLine()) != null) {
            char[] chars = len.toCharArray();
            for (char aChar : chars) {
                //判断数组中的字符是不是大写或者小写字母或者空格
                if ((aChar >= 'a' && aChar <= 'z') || (aChar >= 'A' && aChar <= 'Z') || (aChar == ' ')) {
                    //每多一个字符，总数sum自增
                    sum++;
                    //为了不区分大小写，将大写字母转换成小写字母（根据ASCII编码大写字母变成小写字母需要加32）
                    if (aChar >= 'A' && aChar <= 'Z') {
                        aChar = (char) (aChar + 32);
                    }
                    //判读字符是不是第一次出现
                    if (!map.containsKey(aChar)) {
                        //是第一次出现：将字符当做键，出现的次数1为值，存入Map集合中
                        map.put(aChar, 1);
                    } else {
                        //不是第一次出现：将键对应的值加一
                        Integer integer = map.get(aChar);
                        integer++;
                        map.put(aChar, integer);
                    }
                }
            }
        }
        //关闭资源
        br.close();
        Set<Character> characters = map.keySet();
        double hi = 100;
        for (Character character : characters) {
            //计算概率p
            double p = (double) map.get(character) / (double) sum;
            // 换底公式 ：logx(y)= loge(y)/loge(x)
            //计算信息量Hi
            hi = (Math.log(p) / Math.log(2)) * (-p) + hi;
            System.out.println(character + "的概率为" + p);
        }
        System.out.println("信息熵是" + hi);
    }
}

1111111111111111111111111111111111111